# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:58:48 2020

@author: steve
"""

import time
from fibonacci import fib_sum_even
from count_letters_in_numbers import sum_letters_counting_1_to_1000, sum_letters_counting_1_to
from poker import getHowManyTimes_P1_wins
def main():


    print("\n-------------- PROBLEM 1 ----------------------------")    
    MAX_VAL = 4000000
    
    start_time = time.clock()
    even_sum = fib_sum_even(MAX_VAL)
    end_time   = time.clock() - start_time
    
    print( "Fibonacci Even Sum up to %d :\t %d" % (MAX_VAL, even_sum) )
    print( "Computed in %f seconds"         %  end_time )

    print("-----------------------------------------------------\n\n")


    print("-------------- PROBLEM 2 ----------------------------")    
    start_time    = time.clock()
    character_sum = sum_letters_counting_1_to_1000()
    end_time      = time.clock() - start_time    
    print("Number of Letters used to spell 1 to 1000:\t%d" % character_sum)
    print( "Computed in %f seconds"                         % end_time )

    end_number    = 959
    start_time    = time.clock()
    character_sum = sum_letters_counting_1_to( end_number )
    end_time      = time.clock() - start_time
    print("Number of Letters used to spell 1 to %d:\t%d" % (end_number, character_sum) )
    print( "Computed in %f seconds "                     % end_time )
    print("-----------------------------------------------------\n\n")


    print("-------------- PROBLEM 6 ----------------------------")    
    start_time    = time.clock()
    p1wins        = getHowManyTimes_P1_wins()
    end_time      = time.clock() - start_time    
    print("Number of Times P1 wins in 1000 games:\t%d" % p1wins)
    print( "Computed in %f seconds"                    % end_time )
    print("-----------------------------------------------------\n\n")


    
if __name__ == "__main__":
    main()