# -*- coding: utf-8 -*-
"""
Created on Sun May 24 15:42:25 2020

@author: steve
"""


import time

# numbers

singles = ['','one','two','three','four','five','six','seven','eight','nine']
teens   = ['ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen']
tens    = ['', 'ten','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety']
hundred = 'hundred'


"""
    PROBLEM 2:
    Sum the numbers of letters of the numbers written in words
    from 1 to 1000 (inclusive)
"""

def hello():
    print("EHLLOSDFSSDF")
    
def sum_letters_counting_1_to_1000():
    wordLenSum= 0
        
    #  For 0 to 999
    for k in range(10):
        for j in range(10):
            for i in range(10):
#                number = 100*k + 10*j + i    # counter for debugging
                word = ''                     # initialize to build from empty string
                if k > 0:                     # for numbers > 99
                    word += singles[k] + hundred
                    if j != 0 or i != 0:      # exclude "and" from n % 100 == 0 
                        word += 'and'      #add 'and' for British usage compliance
                if j == 1:     # special case for teen numbers
                    word += teens[i]
                else:          
                    word += tens[j] + singles[i]
                wordLenSum += len(word)
    #            print(number, word, len(word))
                
    wordLenSum += len('onethousand')        # handle 1000 case separately

    return wordLenSum

"""
    Sum the numbers of letters of the numbers writen in words from
    1 to max_val (user defined)
    Maximum allowed value is 1000
    max_val - positive integer 1 to 1000 allowed only
            - if >1000, value is set to 1000
            - if <1,    value is set to 1
            - if not integer, exit function
"""
def sum_letters_counting_1_to( max_val ):
    if not isinstance(max_val, int):
        print("this is not an integer, exiting function")
        return;
    if( max_val < 1 ):
        max_val = 1
        print("Value less than one, setting max_val to 1 ")
    if( max_val > 1000 ):
        max_val = 1000
        print("Value > 1000, setting max_val to 1000")

    wordLenSum = 0      # initialize to zero

#    max_val = 121
    for k in range(10):
        for j in range(10):
            for i in range(10):
                theNumber = 100*k + 10*j + i  # number used for debugging
                word = ''                     # initialize to build from empty string
                if k > 0:                     # for numbers > 99
                    word += singles[k] + hundred
                    if j != 0 or i != 0:      # exclude "and" from n % 100 == 0 
                        word += 'and'      #add 'and' for British usage compliance
                if j == 1:     # special case for teen numbers
                    word += teens[i]
                else:          
                    word += tens[j] + singles[i]
#                print(word)                 #print out word for debugging    
                wordLenSum += len(word)

                if theNumber == max_val:
#                    print("the stopval is:", theNumber)
                    break;
            else:
                continue
            break
        else:
            continue
        break

    if max_val == 1000:
        wordLenSum += len('onethousand')        # handle 1000 case separately

    
#    wordLenSum = 0
    return wordLenSum

#capped at 10,000
# doesnt loop thousands unnecessarily
    
# NOT WORKING
def sum_letters_counting_1_to_over1K( max_val ):
    if not isinstance(max_val, int):
        print("this is not an integer, exiting function")
        return;
    if( max_val < 1 ):
        max_val = 1
        print("Value less than one, setting max_val to 1 ")
    if( max_val > 10000 ):
        max_val = 10000
        print("Value > 10000, setting max_val to 10000")
    if( max_val > 1000 ):
        thousands_incrementer =  max_val // 1000
        print( thousands_incrementer )
    else: 
        print("less than a thousand")
        thousands_incrementer =  max_val // 1000
        print( 'thousands_incrementer', thousands_incrementer )

    
        
    wordLenSum = 0      # initialize to zero
    
    for jj in range(2):
        print("TIEMS")
    
    print("THE STOP VAL IS" , max_val)
    for ii in range(thousands_incrementer):
        for k in range(10):
            for j in range(10):
                for i in range(10):
                    theNumber = 1000*ii + 100*k + 10*j + i  # number used for debugging
                    if( theNumber > 998):
                        print("big ", theNumber)
#    max_val = 121
    for ii in range(thousands_incrementer):
        for k in range(10):
            for j in range(10):
                for i in range(10):
                    theNumber = 1000*ii + 100*k + 10*j + i  # number used for debugging
#                    print('i=', theNumber)
                    word = ''                     # initialize to build from empty string
#                    if ii > 1:
                    if theNumber > 998:   # because 999th element is '1000'
#                        if not ii == 0:
                        word += singles[ii+1] + 'thousand'        # add thousand
#                        pass
                    if k > 0:                     # for numbers > 99
                        word += singles[k] + hundred
                        if j != 0 or i != 0:      # exclude "and" from n % 100 == 0 
                            word += 'and'      #add 'and' for British usage compliance
                    if j == 1:     # special case for teen numbers
                        word += teens[i]
                    else:          
                        word += tens[j] + singles[i]
    #                print(word)                 #print out word for debugging    
                    if theNumber > 998:
                        print("word right before sum ", word)

                    wordLenSum += len(word)
    
                    if theNumber == max_val:
                        print("$$$$$$$$$$$$$the stopval is:", theNumber)
                        return wordLenSum

    return wordLenSum




def main():

    start_time    = time.clock()
    character_sum = sum_letters_counting_1_to_1000()
    end_time      = time.clock() - start_time    
    print("Number of Letters used to spell 1 to 1000:", character_sum)
    print( "Computed in %f seconds ---" % (time.clock()- start_time))
    
    end_number    = 1000
    start_time    = time.clock()
    character_sum = sum_letters_counting_1_to( end_number )
    end_time      = time.clock() - start_time
    print("Number of Letters used to spell 1 to %d:\t%d" % (end_number, character_sum) )
    print( "Computed in %f seconds ---"                  % end_time )

    end_number    = 1002
    start_time    = time.clock()
    character_sum = sum_letters_counting_1_to_over1K( end_number )
    end_time      = time.clock() - start_time
    print("Number of Letters used to spell 1 to (1k and up) %d:\t%d" % (end_number, character_sum) )
    print( "Computed in %f seconds ---"                  % end_time )

if __name__ == "__main__":
    main()

       