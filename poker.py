# -*- coding: utf-8 -*-
"""
Created on Sun May 24 15:51:10 2020

@author: steve
"""
#!/usr/bin/python3
import time


cardValues = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
suit       = ['C','H','D','S']
hierarchy  = ['highCard', 'onePair', 'twoPair', 'threeOfaKind', 'straight', 'flush', 'fullHouse', 'fourOfaKind', 'straightFlush', 'RoyalFlush' ]
            #    0,           1,        2,        3,        4,               5,         6,       7,             8,             , 9 ,            10           


class Poker(object):
    
    def __init__(self):
        self.hand  = []
        self.face  = []
        self.value = []
        self.suit  = []
        self.score = 0
        self.wins  = 0
    
    #  parses out the 5 card hand and parses into list of:
    #       face (char), value (int), suit (char)
    def readHand(self, hand):
        self.hand = hand
        self.face  = [f for f,s in hand]
        self.value = [cardValues.index(f) for f,s in hand ] 
        self.suit  = [s for v,s in hand]
    
    # printout info for debugging
    def showCards(self):
        print( "face",  self.face)
        print( "value", self.value)
        print( "suit",  self.suit)
    
    #  using score, enumerate what hand was computed 
    def show_hand(self):
        print("Score is : ", hierarchy[ self.score ] )

    def getPairs(self):
        return [v for v in set(self.value) if self.value.count(v) == 2]
    
    def getTriple(self):
        return [v for v in set(self.value) if self.value.count(v) == 3]
    
    def getQuads(self):
        return [v for v in set(self.value) if self.value.count(v) == 4]
        
    # algorithm for detecting consecutive numbers
    def check_for_straight(self):
        return sorted( self.value ) == list(range(min(self.value), max(self.value)+1)) 
    
    def check_for_flush(self):
        if len( set(self.suit) ) == 1:
            return True
        else:
            return False

    #get rid of number occurring in self.value; 
    #used in evaluating kicker from pair/twopair/threeofakind/fourofakind
    def remove_number_from_value(self, number):
        self.value = [v for v in self.value if v != number]
        
    # checks what hand the poker cards are and assigns a score WRT hierarchy variable    
    def check_hand(self):
        pairs      = self.getPairs()
        triples    = self.getTriple()
        quads      = self.getQuads()        
        isFlush    = self.check_for_flush()
        isStraight = self.check_for_straight()
        
        # Check for Pair
        if len(pairs) == 1:
            self.score = 1
        # Check for two pair
        elif len(pairs) == 2:
            self.score = 2
        # Check for three of a kind
        elif len(triples) == 1 and len(pairs) == 0:
            if len(pairs) == 0:
                self.score = 3
            elif len(pairs) == 1:
                self.score = 6
        # Check for four of a kind
        elif len(quads) == 1:
            self.score = 7 
        # Check for ...
        elif isStraight:
            # straight
            if not isFlush:
                self.score = 4
            # flush
            elif isFlush:
                self.score = 8
                # staight flush
                if max( self.value ) == 12:    # corresponds to ACE HIGH AKA ROYAL FLUSH
                    self.score = 9
        # Check for flush
        elif isFlush and not isStraight:
            self.score = 5
        # Check for high card
        else:
            self.score = 0
    
    # determine winner using "score"
    # use evaluate_similar_hands() if more detailed analysis needed
    @staticmethod
    def evaluate_hands( p1, p2):
        if p1.score > p2.score:
            p1.wins += 1
        elif p1.score < p2.score:
            p2.wins += 1
        else:
            Poker.evaluate_similar_hands( p1, p2 )

    
    # Use this method when the scores match
    # to determine which hand is better 
    @staticmethod            
    def evaluate_similar_hands( p1, p2 ):

        # choose either since this happens only if p1.score = p2.score
        score = p1.score   

        p1pair   = p1.getPairs()
        p2pair   = p2.getPairs()
#        print( 'pairs p1,p2', p1pair, p2pair)
        if score == 0:      #  high card 
            Poker.evaluate_highest_kicker(p1,p2)                
            return
        if score == 1:      #  one pair
            pairDiff = p1pair[0] - p2pair[0]
            if pairDiff > 0:
                p1.wins += 1
            elif pairDiff < 0:
                p2.wins += 1
            elif pairDiff == 0:
                p1.remove_number_from_value( p1pair[0] )
                p1.remove_number_from_value( p2pair[0] )
#                print('p1, p2value', p1.value, p2.value )
                Poker.evaluate_highest_kicker(p1,p2)                
            return
        if score == 2:      #  two pair
            topPairComp = max(p1pair) - max(p2pair)
            if topPairComp > 0:
                p1.wins += 1
            elif topPairComp < 0:
                p2.wins += 1
            else:
                bottomPairComp = min(p1pair) - min(p2pair)
                if bottomPairComp > 0:
                    p1.wins +=1
                elif bottomPairComp < 0:
                    p2.wins +=1
                else:
                    [ p1.remove_number_from_value(i) for i in p1pair ]
                    [ p2.remove_number_from_value(i) for i in p2pair ]
                    Poker.evaluate_highest_kicker(p1,p2)
            return
        if score == 3:      # three of a kind
            p1triple = p1.getTriple()
            p2triple = p2.getTriple()
            tripleComp = p1triple[0] - p2triple[0]
            if tripleComp > 0:
                p1.wins += 1
            elif tripleComp < 0:
                p2.wins += 1
            else:
                p1.remove_number_from_value(p1triple[0])
                p2.remove_number_from_value(p2triple[0])
                Poker.evaluate_highest_kicker(p1,p2)            
            return
        if score == 4:      #  straight
            straightComp = max( p1.value ) - max( p2.value )
            if straightComp > 0:
                p1.wins += 1
            elif straightComp < 0:
                p2.wins += 1
            else:
                pass
            return
        if score == 5:      #  flush
            Poker.evaluate_highest_kicker(p1,p2)
            return
        if score == 6:      #  full house
            tripleComp = p1.getTriple()[0] - p2.getTriple()[0]
            if tripleComp > 0:
                p1.wins += 1
            elif tripleComp < 0:
                p2.wins += 1
            else:
                pairComp = p1pair - p2pair
                if pairComp > 0:
                    p1.wins += 1
                elif pairComp < 0:
                    p2.wins += 1
                else:
                    pass
            return
        if score == 7:      #  four of a kind
            p1quads = p1.getQuads()[0]
            p2quads = p2.getQuads()[0]
            quadComp = p1quads - p2quads
            if quadComp > 0:
                p1.wins += 1
            elif quadComp < 0:
                p2.wins += 1
            else:
                p1.remove_number_from_value(p1quads)
                p2.remove_number_from_value(p2quads)
                Poker.evaluate_highest_kicker(p1,p2)
            return
        if score == 8:      #  straight flush
            sfComp = max(p1.value) - max(p2.value)
            if sfComp > 0:
                p1.wins += 1
            elif sfComp < 0:
                p2.wins += 1
            else:
                pass
            return
        if score == 9:      #  royal flush
            print("There will never be two royal flushes")
            return


    #    The next Highest cards are what determine the winner:
    #    ASSUMES poker hands are correct: no duplicates hand exist & there is always an answer 
    #    ADD:  Loop until there are no more cards in hand (will slow down code)
    @staticmethod
    def evaluate_highest_kicker( p1, p2):
        # save a copy since list will get modified
        p1val = p1.value
        p2val = p2.value
        p1val.sort()
        p2val.sort()

        kickerDiff = max( p1val) - max(p2val)
        while( kickerDiff == 0 ):
            
            p1val.pop()
            p2val.pop()
            kickerDiff = max(p1val) - max(p2val)
            print( p1val, p2val, kickerDiff)
        
        if kickerDiff > 0:    # p1 has the highest kicker
            p1.wins +=1
        elif kickerDiff < 0:  # p2 has the highest kicker
            p2.wins += 1
        else:                 # its a draw 
            pass 
        

    @staticmethod
    def show_scores( p1, p2 ):
        print( "p1,p2 scores:",  p1.score, p2.score)
    
    @staticmethod
    def show_wins( p1, p2 ):
        print( "p1,p2 wins", p1.wins, p2.wins )

    @staticmethod
    def show_cards( p1, p2 ):
        print("p1: ", p1.hand)
        print("p2: ", p2.hand)


def format_data( fileName ):
    with open(fileName) as file:
        data = file.read()
    
    delimited = data.split("\n")
    p1 = [ line.split(" ")[ 0:5 ] for line in delimited]
    p2 = [ line.split(" ")[ 5:10] for line in delimited]

    # pop() because last data point is empty due to extra ","
    p1.pop(); p2.pop() 
    return p1, p2

def getHowManyTimes_P1_wins():
    p1, p2 = format_data( 'poker.txt' )

    # Instantiate players 
    player1 = Poker()
    player2 = Poker()

#    print("++++++++++++++++++++++++++++++++++")
    for i in range( 0, len(p1)):
#        print("i", i)
        player1.readHand( p1[i] )
        player2.readHand( p2[i] )
        player1.check_hand()
        player2.check_hand()
        
        Poker.evaluate_hands( player1, player2 )

    return player1.wins
    

"""
converts to integers relating to index value of cardValues
"""
def main():

#    manual_test_cases()
    start_time    = time.clock()
    p1wins        = getHowManyTimes_P1_wins()
    end_time      = time.clock() - start_time    
    print("Number of Hands P1 won in 1000 poker hands :", p1wins)
    print( "Computed in %f seconds ---" % end_time)


    
# make sure order of Two pair hands properly evaluated 
def test_case_two_pair():
    twopair_hi            = ['2C', '2H', 'KD', 'KS', '3C']
    twopair_lo            = ['5C', '5H', 'QD', 'QS', '2C']
    twopair_samehi_hi     = ['6C', '6H', 'KD', 'KS', '2C'] 
    twopair_samehi_lo     = ['2C', '2H', 'KD', 'KS', '4C']
    twopair_onlyKicker_hi = ['5C', '5H', 'TD', 'TS', 'AC'] 
    twopair_onlyKicker_lo = ['5C', '5H', '9D', '9S', '3C']
    
    player1 = Poker()
    player2 = Poker()

    print("___________________________________")
    print(" Two pair: ")
    player1.readHand(twopair_hi)
    player1.check_hand()
    
    player2.readHand(twopair_lo)
    player2.check_hand()

    Poker.evaluate_similar_hands( player1, player2 )
    Poker.show_cards(player1, player2)
    Poker.show_wins(player1, player2)
    
    print("___________________________________")
    print(" Two pair, same hi: ")
    player1.readHand(twopair_samehi_hi)
    player1.check_hand()
    
    player2.readHand(twopair_samehi_lo)
    player2.check_hand()

    Poker.evaluate_similar_hands( player1, player2 )
    Poker.show_cards(player1, player2)
    Poker.show_wins(player1, player2)
    print("___________________________________")

    print(" Two pair, only kicker : ")
    player1.readHand(twopair_onlyKicker_hi)
    player1.check_hand()
    
    player2.readHand(twopair_onlyKicker_lo)
    player2.check_hand()

    Poker.evaluate_similar_hands( player1, player2 )
    Poker.show_cards(player1, player2)
    Poker.show_wins(player1, player2)
    print("___________________________________")
    
    
    
    
#  Make sure kickers are properly determined     
def test_case_similar_one_pair():
    onepair_worst_case_hi =  ['3C', '4H', '6D', '7S', '7C']
    onepair_worst_case_lo =  ['2C', '4H', '6D', '7S', '7C']
    
    onepair_hi =  ['2C', '4H', '6D', 'KS', 'KC']
    onepair_lo =  ['3C', '5H', '2D', 'JS', 'JC']

    player1 = Poker()
    player2 = Poker()

    player1.readHand(onepair_worst_case_hi)
    player1.check_hand()
    player1.show_hand()
    
    player2.readHand(onepair_worst_case_lo)
    player2.check_hand()
    player2.show_hand()

    Poker.evaluate_similar_hands( player1, player2 )
    print('one pair worst case scenario' )
    print("Wins p1,p2:", player1.wins, player2.wins )

    player1.readHand(onepair_worst_case_hi)
    player1.check_hand()
    player1.show_hand()
    
    player2.readHand(onepair_worst_case_lo)
    player2.check_hand()
    player2.show_hand()


    print(' one pair, one has higher pair')
    print("Wins p1,p2:", player1.wins, player2.wins )




def test_case_similar_high_hand():
    player1 = Poker()
    player2 = Poker()

    highcard_worst_case_hi = ['3C', '4H', 'JC', '6D', '8S']
    highcard_worst_case_lo = ['2C', '4H', 'JC', '6D', '8S']
    
#    highcard_j_hi
    player1.readHand(highcard_worst_case_hi)
    player1.check_hand()
    player1.show_hand()
    
    player2.readHand(highcard_worst_case_lo)
    player2.check_hand()
    player2.show_hand()

    Poker.evaluate_similar_hands( player1, player2 )
    print("Wins p1,p2:", player1.wins, player2.wins )
    
def manual_test_cases():

#    test_case_similar_high_hand()
#    test_case_similar_one_pair()
#    test_case_two_pair()


    highcard_a        = ['2C','4H','6D','8S','AH']
    highcard_j_hi     = ['2C','4H','6D','9S','JH']
    highcard_j_lo     = ['2C','4H','6D','TS','JH']
    
    pairhand_9        = ['9D0', '9H', '5D', '2C', '3S']   
    pairhand_7_hi     = ['7D', '7H', 'AD', '2C', '3S']    #A kicker
    pairhand_7_lo     = ['7D', '7H', '5D', '2C', '3S']    #5 kicker
    
    twoPair_5_6_hi    = ['5C', '5D', '6C', '6H', 'AS']   # A kicker
    twoPair_5_6_lo    = ['5C', '5D', '6C', '6H', '7S']   # 7 kicker
    
    threeOfaKind_5_hi = ['5D', '5H', '5C', 'AC', '3S'] #A kicker
    threeOfaKind_5_lo = ['5D', '5H', '5C', '2C', '3S'] #3 kicker
    
    fourOfaKind_6_hi  =  ['6C', '6H', '6S', '6D', 'KD']  #K kicker
    fourOfaKind_6_lo  =  ['6C', '6H', '6S', '6D', '2D']  #2 kicker
    
    straight_9_K =  ['9D', 'TH', 'JC', 'QS', 'KS']
    straight_3_7 =  ['3D', '4H', '5C', '6S', '7S']
    
    flush_A_hi    =  ['3D', '5D', 'QD', 'AD', 'TD']
    flush_8_hi    =  ['3D', '2D', '6D', '8D', '7D']
    flush_K_hi    =  ['3D', '5D', '8D', 'JD', 'KD']    # K hi, next kicker J
    flush_K_hi_lo =  ['3D', '5D', '8D', 'TD', 'KD']    # K hi, next kicker T
    
    
    straight_flush_9_k = ['9H','TH','JH','QH','KH']
    straight_flush_3_7 = ['3H','4H','5H','6H','7H']
    
    royal_flush      = ['TS','JS','QS','KS','AS']

    pairhand_9        = ['9D', '9H', '5D', '2C', '3S']   
    player1 = Poker()
    
    player1.readHand(pairhand_9)
    player1.showCards()
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(pairhand_7_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(twoPair_5_6_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(threeOfaKind_5_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(fourOfaKind_6_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(pairhand_7_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(straight_9_K)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(straight_9_K)
    player1.check_hand()
    player1.show_hand()
    
    
    player1.readHand(flush_8_hi)
    player1.check_hand()
    player1.show_hand()
    
    
    player1.readHand(straight_flush_3_7)
    player1.check_hand()
    player1.show_hand()
    
    
    player1.readHand(royal_flush)
    player1.showCards()
    player1.check_hand()
    player1.show_hand()
    
    
    
    player1.readHand(highcard_a)
    player1.check_hand()
    player1.show_hand()
    
    
    player1.readHand(highcard_j_hi)
    player1.check_hand()
    player1.show_hand()
    
    player1.readHand(highcard_j_lo)
    player1.check_hand()
    player1.show_hand()
    
    
if __name__ == "__main__":
    main()
