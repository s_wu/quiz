# -*- coding: utf-8 -*-
"""
Created on Sun May 24 13:39:52 2020

@author: steve
"""

# Problem 1
# Fibonnaci : Sum the even-valued terms below 4,000,000

import time

#parity = ['both', 'odd', 'even']

"""
    Computes the sum of EVEN values in Fibonacci Sequence (starting at 0,1)
    up to max_value
    max_value - enter any positive integer up to python int limit
"""
def fib_sum_even_4M(  ):
    sum = 0
    a = 0
    b = 1
    c = a + b        
    
    while(1):
        c = a + b
        if( c > 4e6 ):
            break;
        a = b
        b = c
#        print("!!", c)
        if ( c % 2 == 0 ):
            sum += c
    return sum
#print("Sum = ", sum )


def fib_sum_even( max_value ):
    sum = 0
    a = 0
    b = 1
    c = a + b        

    while(1):
        c = a + b
        if( c > max_value ):
            break;
        a = b
        b = c
#        print("!!", c)
        if ( c % 2 == 0 ):
            sum += c
    return sum
#print("Sum = ", sum )

def incr(count):
    count += 1

#if parity == 'odd',         add all odd values to max_value
#if parity == 'even',        add all even values to max_value
#if parity == anything else, add all values to max value 
    
def fib_sum( parity, max_value ):
    sum = 0
#    MAX_VAL = 4000000
    a = 0
    b = 1
    c = a + b        

    if parity == 'odd':
        selector = 1
        
    elif parity == 'even':
        selector = 0
        pass
    else:
        while(1):
            c = a + b
            if( c > max_value):
                break;
            a = b
            b = c
            sum += c
        return sum
    
    while(1):
        c = a + b
        if( c > max_value):
            break;
        a = b
        b = c
#        print("!!", c)
#        if parity == 'even':
            
#            sum += c if even
        if selector:
            if ( c % 2 == 1 ):
                sum += c
        else:
            if( c % 2 == 0 ):
                sum += c
        
    return sum
#print("Sum = ", sum )

def main():
    
    MAX_VAL = 2000000
    print('-----------------------------------')
    start_time = time.clock()
    even_sum = fib_sum_even_4M()
    end_time   = time.clock() - start_time
    
    print( "fib_sum_even_4M :\t %d" % ( even_sum) )
    print( "Computed in %e seconds ---"         %  end_time )
    print('-----------------------------------')
    
    start_time = time.clock()
    even_sum = fib_sum_even(MAX_VAL)
    end_time   = time.clock() - start_time
    
    print( "fib_sum_even(%d) :\t %d" % ( MAX_VAL, even_sum) )
    print( "Computed in %e seconds ---"         %  end_time )



    print("===================================")

    print('-----------------------------------')
    start_time = time.clock()
    parity = 'odd'
    fibSumOdd = fib_sum(parity, MAX_VAL)
    end_time   = time.clock() - start_time
    
    print( "fib_sum(%s, %d) :\t %d" % ( parity, MAX_VAL, fibSumOdd) )
    print( "Computed in %e seconds ---"         %  end_time )


    print('-----------------------------------')
    
    start_time = time.clock()
    parity = 'even'
    fibSumEven = fib_sum(parity, MAX_VAL)
    end_time   = time.clock() - start_time
    
    print( "fib_sum(%s, %d) :\t %d" % ( parity, MAX_VAL, fibSumEven) )
    print( "Computed in %e seconds ---"         %  end_time )


    print('-----------------------------------')
    
    start_time = time.clock()
    parity = 'both'
    fibSumBoth = fib_sum(parity, MAX_VAL)
    end_time   = time.clock() - start_time
    
    print( "fib_sum(%s, %d) :\t %d" % ( parity, MAX_VAL, fibSumBoth) )
    print( "Computed in %e seconds ---\n\n"         %  end_time )


    if fibSumEven+fibSumOdd == fibSumBoth:
        print('fibSumEven+fibSumOdd == fibSumBoth' )
    else:
        print('fibSumEven+fibSumOdd != fibSumBoth' )



if __name__ == "__main__":
    main()
